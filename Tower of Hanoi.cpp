#include <iostream>

using namespace std;

int moves(0);
void Hanoi(int m, char a, char b, char c);

void Hanoi(int m, char a, char b, char c){
  moves++;
  if(m == 1){
    cout << "The disc " << m << " move from " << a << " to " << c << endl;
  }else{  
    Hanoi(m-1, a,c,b);
    cout << "The disc " << m << " move from " << a << " to " << c << endl;
    Hanoi(m-1,b,a,c);
  }
}

int main(){

  int discs;
  cout << "Enter the number of discs: " << endl;
  cin >> discs;
  cout<<endl;
  Hanoi(discs, 'A', 'B', 'C');
  cout <<endl<< "It took " << moves << " moves to reach it's target. " << endl<<endl;

  system("pause");
}